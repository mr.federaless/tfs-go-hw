package main

import (
	"encoding/json"
	"flag"
	"github.com/pkg/errors"
	"io/ioutil"
	"sort"
	"time"
	"log"
)

type NewsItem struct {
	ID          int64     `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	Provider    string    `json:"provider"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
}
type Feed struct {
	Type    string      `json:"type"`
	Payload interface{} `json:"payload"`
}
type FeedNews struct {
	Items       []NewsItem `json:"items"`
	PublishedAt time.Time  `json:"published_at"`
	Tickers     []string   `json:"tickers"`
}

func main() {
	log.Println("starting...")
	var fileName string
	flag.StringVar(&fileName, "file", "news.json", "file must be specified")
	flag.Parse()

	news, err := parseNewsFile(fileName)
	if err != nil {
		log.Fatalf("error parsing file", err)
	}

	out := makeFeedData(news)

	err = makeOutJson(out, "out.json")
	if err != nil {
		panic(errors.Wrap(err, "error writing file to out.json"))
	}
}
func makeFeedData(news []NewsItem) []Feed {
	dailyNewsMap := make(map[time.Time][]NewsItem)
	var dates []time.Time

	for i := range news {
		mapKey := getStartOfDay(news[i].PublishedAt)
		if len(dailyNewsMap[mapKey]) == 0 {
			dates = append(dates, mapKey)
		}
		dailyNewsMap[mapKey] = append(dailyNewsMap[mapKey], news[i])
	}

	sort.Slice(dates, func(i, j int) bool {
		return dates[i].Before(dates[j])
	})

	var out []Feed
	for _, day := range dates {
		feed := Feed{}
		currentValues := dailyNewsMap[day]

		sort.Slice(currentValues, func(i, j int) bool {
			return currentValues[i].PublishedAt.Before(currentValues[j].PublishedAt)
		})
		alreadyProcessedMap := make(map[int64]bool)
		for _, value1 := range currentValues {

			var news []NewsItem
			news = append(news, value1)

			for _, value2 := range currentValues {
				if alreadyProcessedMap[value2.ID] {
					continue
				}
				if value1.PublishedAt.Before(value2.PublishedAt) && compareSlices(value1.Tickers, value2.Tickers) {
					news = append(news, value2)
					alreadyProcessedMap[value1.ID] = true
					alreadyProcessedMap[value2.ID] = true
				}
			}

			if len(news) > 1 {
				feed.Type = "company_news"
				feed.Payload = FeedNews{news, news[0].PublishedAt, news[0].Tickers}
			} else {
				if alreadyProcessedMap[news[0].ID] {
					continue
				}
				feed.Type = "news"
				feed.Payload = news[0]
			}
			out = append(out, feed)
		}
	}

	return out
}
func parseNewsFile(fileName string) ([]NewsItem, error) {
	byteData, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, errors.Wrap(err, "unable to load file "+fileName)
	}

	news := make([]NewsItem, 0)
	err = json.Unmarshal(byteData, &news)
	if err != nil {
		return nil, errors.Wrap(err, "error parsing json")
	}
	return news, nil
}
func makeOutJson(data []Feed, filename string) error {
	outJson, err := json.MarshalIndent(data, "", "    ")
	if err != nil {
		return errors.Wrap(err, "error marshalling data")
	}

	err = ioutil.WriteFile(filename, outJson, 0644)
	if err != nil {
		return errors.Wrap(err, "error while writing to file")
	}
	return nil
}
func compareSlices(a []string, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i := 0; i < len(a); i++ {
		var contains bool
		for j := 0; j < len(b); j++ {
			if a[i] == b[j] {
				contains = true
			}
		}
		if !contains {
			return false
		}
	}
	return true
}
func getStartOfDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}
