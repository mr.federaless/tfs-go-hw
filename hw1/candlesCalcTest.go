package main

import (
	"os"
	"fmt"
	"bufio"
	"strings"
)

func main(){
	args := os.Args[1:]
	if	len(args) != 2 {
		fmt.Errorf("check/answer file(s) not specified, usage: candlesCalcTest <check.file> <answer.file>")
	}

	checkFileName := args[0]
	answerFileName := args[1]
	fmt.Println("check, answer", checkFileName, answerFileName)

	checkDataFile, err := os.Open(checkFileName)
	defer checkDataFile.Close()
	if err != nil {
		fmt.Errorf("no such file", err)
	}

	answerDataFile, err := os.Open(answerFileName)
	defer answerDataFile.Close()
	if err != nil {
		fmt.Errorf("no such answer file", err)
	}

	checkReader := bufio.NewReader(checkDataFile)
	checkLines := make([]string, 26, 26)

	for {
		line, err := checkReader.ReadString('\n')

		if strings.Contains(line, "Ticker") {
			continue
		}

		checkLines = append(checkLines, line)

		if err != nil {
			break
		}
	}

	answerReader := bufio.NewReader(answerDataFile)
	answerLines := make([]string, 26, 26)

	for {
		line, err := answerReader.ReadString('\n')

		answerLines = append(answerLines, line)

		if err != nil {
			break
		}
	}

	fmt.Printf("length of chk=%v, ans=%v\n", len(checkLines), len(answerLines))

	var moreSlice *[]string
	var lessSlice *[]string
	if len(checkLines) < len(answerLines) {
		moreSlice = &answerLines
		lessSlice = &checkLines
	} else {
		moreSlice = &checkLines
		lessSlice = &answerLines
	}
	diffSlice := make([]string, 10,10)//todo add actual length calculateion
	//iteration over less slice to make sure every index is present in more slice
	for _,v := range *lessSlice {
		var contains bool
		//todo resolve, embedded for
		for _, v1 := range *moreSlice {
			if	strings.TrimSpace(v) == strings.TrimSpace(v1) {
				contains = true
			}
		}
		if !contains {
			diffSlice = append(diffSlice, v)
		}
	}
	//todo add case on different length
	fmt.Println("diff: ", diffSlice)


}
