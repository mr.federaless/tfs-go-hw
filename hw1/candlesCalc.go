package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"github.com/jszwec/csvutil"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

type TradeData struct {
	Ticker   string
	Price    float64
	Count    int
	DateTime Time
}

type CandleData struct {
	Ticker   string
	DateTime Time
	Start    float64
	Max      float64
	Min      float64
	End      float64
}
type Time struct {
	time.Time
}

const DATE_TIME_FORMAT string = "2006-01-02 15:04:05.000000"

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func (t Time) MarshalCSV() ([]byte, error) {
	var b [len(time.RFC3339)]byte
	return t.AppendFormat(b[:0], time.RFC3339), nil
}

func (t *Time) UnmarshalCSV(data []byte) error {
	tt, err := time.Parse(DATE_TIME_FORMAT, string(data))
	if err != nil {
		return err
	}
	*t = Time{Time: tt}
	return nil
}

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Println("No filename specified. Usage: `candlesCalc <path_to_trades_file>")
		return
	}

	data := collectTradeData(args[0])

	b := parseCandleData(5, 7*60, 24*60, data)
	storeCandleData(b, "candles_5min.csv")

	b = parseCandleData(30, 7*60, 24*60, data)
	storeCandleData(b, "candles_30min.csv")

	b = parseCandleData(240, 7*60, 24*60, data)
	storeCandleData(b, "candles_240min.csv")
}

func collectTradeData(filename string) []TradeData {
	data, err := ioutil.ReadFile(filename)
	check(err)

	header, err := csvutil.Header(TradeData{}, "csv")
	check(err)

	csvReader := csv.NewReader(strings.NewReader(string(data)))

	dec, err := csvutil.NewDecoder(csvReader, header...)
	check(err)

	var trades []TradeData

	for {
		var t TradeData

		if err := dec.Decode(&t); err == io.EOF {
			break
		}
		check(err)
		trades = append(trades, t)
	}
	return trades
}

func parseCandleData(span int, startMin int, endMin int, trades []TradeData) []*CandleData {
	var result []*CandleData

	m := make(map[string]*CandleData)
	var curMin = startMin

	for _, v := range trades {
		minutes := v.DateTime.Hour() * 60 + v.DateTime.Minute()

		if minutes < startMin {
			curMin = startMin
			continue
		}

		if minutes >= curMin + span {
			curMin += span
		}

		dur, _ := time.ParseDuration(strconv.Itoa(curMin) + "m")
		mapKey := v.Ticker + v.DateTime.Truncate(time.Hour * 24).Add(dur).String()

		if val, ok := m[mapKey]; ok {
			val.End = v.Price
			if val.Min > v.Price {
				val.Min = v.Price
			}
			if val.Max < v.Price {
				val.Max = v.Price
			}
		} else {
			cd := CandleData{v.Ticker, Time{v.DateTime.Truncate(time.Hour * 24).Add(dur)}, v.Price, v.Price, v.Price, v.Price}

			m[mapKey] = &cd
			result = append(result, &cd)
		}
	}
	return result
}

func storeCandleData(data []*CandleData, filename string) {
	b, err := csvutil.Marshal(data)
	check(err)

	fmt.Println(string(b))
	f, err := os.Create(filename)
	check(err)
	defer f.Close()

	w := bufio.NewWriter(f)
	defer w.Flush()
	_, err = w.WriteString(string(b))
	check(err)
}
